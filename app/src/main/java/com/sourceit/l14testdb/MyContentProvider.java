package com.sourceit.l14testdb;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by alexeyburkun on 6/17/17.
 */

public class MyContentProvider extends ContentProvider{

    private static final String AUTHORITY = BuildConfig.AUTHORITY;
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    private final NotesTable notesTable = new NotesTable();

    static {
        URI_MATCHER.addURI(AUTHORITY, NotesTable.NAME, MappedUri.NOTES);
        URI_MATCHER.addURI(AUTHORITY, NotesTable.NAME + "/#", MappedUri.NOTES_ITEM);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        database = new DataBaseHelper(getContext()).getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Table table = getTable(uri);
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(table.getName());
        if(isItemUri(uri)){
            builder.appendWhere(table.getColumnId() + "=" + uri.getLastPathSegment());
        }
        Cursor cursor = builder.query(database, projection,selection, selectionArgs, null, null, sortOrder);
        setNotificationUri(cursor, uri);
        return cursor;
    }

    private void setNotificationUri(Cursor cursor, Uri uri) {
        Context context = getContext();
        if(context != null){
            cursor.setNotificationUri(context.getContentResolver(), uri);
        }
    }

    private boolean isItemUri(Uri uri) {
        switch (URI_MATCHER.match(uri)){
            case MappedUri.NOTES_ITEM:
        }

        return true;
    }

    private Table getTable(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case MappedUri.NOTES:
            case MappedUri.NOTES_ITEM:
                return notesTable;

            default:
                throw new IllegalArgumentException("Uri: " + uri + " is not supported");
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public interface MappedUri{
        int NOTES= 0;
        int NOTES_ITEM = 1;
    }
}
